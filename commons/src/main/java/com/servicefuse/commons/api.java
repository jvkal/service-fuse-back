package com.servicefuse.commons;


import com.servicefuse.commons.annotation.StartSwaggerScan;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: xuzh42212
 * @Description: TODO
 * @DateTime: 2023/10/30 23:21
 **/
@RestController
@RequestMapping("/servicefuse/testApi")
@StartSwaggerScan
@Slf4j
public class api {

    @Value("${test.test}")
    String value;


    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public void accountPageList() {
        System.out.println(value);
        log.info("value");
    }
}
