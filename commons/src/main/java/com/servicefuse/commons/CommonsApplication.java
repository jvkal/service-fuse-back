package com.servicefuse.commons;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @Author: xuzh42212
 * @Description: Commons项目启动类
 * @DateTime: 2023/10/30 12:59
 **/
@SpringBootApplication
@EnableDiscoveryClient
public class CommonsApplication {

    public static void main(String[] args) {
        SpringApplication.run(CommonsApplication.class , args);
    }
}
