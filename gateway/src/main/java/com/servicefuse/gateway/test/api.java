package com.servicefuse.gateway.test;


import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;


/**
 * @Author: xuzh42212
 * @Description: TODO
 * @DateTime: 2023/10/30 23:21
 **/
@RestController
@RequestMapping("/testApi1")
@EnableScheduling
@Slf4j
public class api {

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    @Scheduled(fixedRate = 1 * 60 * 1000)
    public void accountPageList() {
        System.out.println("value");
        log.info("value");
    }
}
