package com.servicefuse.gateway.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: xuzh42212
 * @Description: 测试nacos读取数据
 * @DateTime: 2023/10/30 23:15
 **/
@Configuration
@ConfigurationProperties(prefix = "test")
@Data
public class NacosConfig {
    private String test;
}
